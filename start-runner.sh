#!/bin/bash

#Start automatically the process
gitlab-runner start

#Make sure existing runners in the node are unregistered 
gitlab-runner unregister --all-runners

#Register the new container
gitlab-runner register && sleep 2

