# GitLab Runners in Openshift

This repository contains the steps to follow a simple docker strategy to deploy scalable Gitlab Runners in OCP 3.11

In this deployment is assumed dedicated runners per Gitlab projects, and it can scale up and down allocating one runner per container

_NOTE: It has been tested in OCP 3.11, but it may work in several previous versions as well

## Clone this Repository

Clone this repository using the following command:

```console
$ git clone https://gitlab.com/jfloristan/ocp-gitlab-runners.git
```

## Configure Gitlab CI/CD variables in Dockerfile (Optional)

The preconfigured Dockerfile does not need configuration, is optional, however it assumes your projects will be in https://gitlab.com
and you want your runners tagged as docker and named as ocp-runner. This 3 parameters can be modified in the Dockerfile before
proceeding with the next steps

```console
ENV CI_SERVER_URL https://gitlab.com/
ENV RUNNER_NAME ocp-runner
ENV RUNNER_TAG_LIST docker
```

## Create the ocp-gitlab-runners-service project and ocp-gitlab-runner application in Openshift

```console
$ oc new-project ocp-gitlab-runners-service --description="Gitlab Runners in OCP" --display-name="OCP Gitlab Runners Service" 
$ oc new-app . --name=ocp-gitlab-runner --strategy=docker
```

## Create the secret in Openshift to inject the GitLab CI/CD token into the runner and add the mapping to the application

The <GitLab CI/CD Token> is the token from the project where you plan to execute your pipelines. Go to your Gitlab project dashboard 
and in Settings -> CI/CD -> Runners you will find the token under the section "Set up a specific Runner manually"

```console
$ oc create secret generic gitlab-ci-token --from-literal=REGISTRATION_TOKEN=<GitLab CI/CD Token>
$ oc set env --from=secret/gitlab-ci-token dc/ocp-gitlab-runner
```

## Patch the ocp-gitlab-runner deployment config to add the preStop and postStart hooks in Openshift

```console
$ oc patch dc/ocp-gitlab-runner --patch "`cat container-hooks.yaml`"
```

## Start the build of the runner

```console
$ oc start-build ocp-gitlab-runner --from-dir=. --follow
```

Go to your Gitlab project dashboard and in Settings -> CI/CD -> Runners you should see you active runners under
the section "Runners activated for this project"

You can scale up your pods in Openshift and you will see more runners available in your project. Scale down the pods,
and the list available runners in your project will decrease.
