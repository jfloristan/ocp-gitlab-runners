FROM gitlab/gitlab-runner
MAINTAINER Juanjo Floristan
LABEL License=GPLv2 \
      Version=3.11.0

# Install OC client
RUN wget --quiet -O /tmp/openshift-origin-client-tools-v3.11.0.tar.gz https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz && \
    tar zxvf /tmp/openshift-origin-client-tools-v3.11.0.tar.gz -C /tmp && \
    cp /tmp/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/oc /usr/bin/oc && \
    rm -rf /tmp/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit /tmp/openshift-origin-client-tools-v3.11.0.tar.gz

# Simple startup script to avoid some issues observed with container restart
ADD start-runner.sh /start-runner.sh
RUN chmod -v +x /start-runner.sh

# Configure Runner
RUN mkdir -p /tmp/gitlab_runner/
ENV CI_SERVER_URL https://gitlab.com/
ENV RUNNER_NAME ocp-runner
ENV REGISTER_NON_INTERACTIVE true
ENV CI_RUNNER_TAGS docker
ENV RUNNER_EXECUTOR shell
ENV RUNNER_TAG_LIST docker
ENV REGISTER_RUN_UNTAGGED false
ENV CONFIG_FILE /tmp/gitlab_runner/config.toml

## REGISTRATION_TOKEN should be injected as a secret
## but it can be added directly in the Dockerfile
#ENV REGISTRATION_TOKEN <GITLAB CI TOKEN>
